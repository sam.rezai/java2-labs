public enum Value{
	ZERO,
	ONE,
	TWO,
	THREE,
	FOUR,	
	FIVE,
	SIX,
	SEVEN,
	EIGHT, 
	NINE,
	DRAWTWO,
	DRAWFOUR,
	REVERSE,
	SKIP;
	
	
	public String toString(){
		if (this==ZERO){
			return "0";
		}
		else if (this==ONE){
			return "1";
		}
		else if(this==TWO){
			return "2";
		}
		else if(this==THREE){
			return "3";
		}
		else if(this==FOUR){
			return "4";
		}
		else if(this==FIVE){
			return "5";
		}
		else if(this==SIX){
			return "6";
		}
		else if(this==SEVEN){
			return "7";
		}
		else if(this==EIGHT){
			
			return "8";
		}
		else if(this==NINE){
			
			return "9";
		}
		else if(this==DRAWTWO){
			return "+2";
		}
		else if(this==DRAWFOUR){
			return "+4";
		}
		else if(this==REVERSE){
			return "REVERSE";
		}
		else if(this==SKIP){
			return "Skip";
		}
		
		else{
			return "Empty";
		}
		
	}
}