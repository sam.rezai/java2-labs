public class Card{
	
	private Value value;
	private Color color;
	
	//make a constructor
	public Card(Value value, Color color){
			
		this.value= value;
		this.color= color;
		
	}
	
	public String toString(){
			
		String card =""+this.color +","+ this.value;
		
		return card;
	}
	
	public Value getValue(){
		return this.value;	
	}

	public Color getColor(){
		return this.color;
	}

	//toString
	
	//notice the attributes are private: add getters and setters
	
}