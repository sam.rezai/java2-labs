import java.util.Random;
public class DynamicCardArray{
	
  private Card[] card;
  private int pointer; 
  
  //hands, and discard because these should start empty
  public DynamicCardArray(){
    this.card = new Card[108];
    this.pointer = 0; 
  }
  
  //making a deck -> the deck should be full
  //generateDeck gives us a full deck of cards. 
  public DynamicCardArray(Card[] deck){
	  this.card = deck;
	  this.pointer = deck.length;
  }
  
  //adding an element into the hand
  public void add(Card c){
	  
    this.card[pointer] = c;
	
    pointer++;
  }

  public Card getTopCard(){
    return this.card[pointer-1];
  }



   // looking if deck isEmpty or not
	public boolean isEmpty(){
		
	return pointer == 0;		
	}
  
	  //removing an element from the hand 
  public Card removeCardFromDeck(){
    if(isEmpty()){
      throw new IllegalArgumentException("The Deck is Empty :(");
    }
    else{
      pointer--;
      Card stringToRemove = card[pointer];
      card[pointer] = null;
      return stringToRemove; 
    }
  }
 
    //remove a specific string/element
   public Card removeAtIndex(int index){
	   
    Card cardToReturn = this.card[index];
    for(int i=index+1;i<pointer;i++){
      this.card[i-1]=card[i];
    }
    pointer--;
    this.card[pointer]=null;
    return cardToReturn;
  }
	public Card[] drawWild(int index){
		
		if (index <0){
			throw new IllegalArgumentException("Must put possitive cards, but try to draw "+ index+ " cards.");
		}
		if (index > pointer){
			throw new IllegalArgumentException("You cannot draw "+ index+" cards since there are only " +pointer+" cards." );
		}
		Card[] draw= new Card[index];
		
		for (int i =0; i< index; i++){
			draw[i]= card[pointer];
		}
		return draw;
		
	}
    public Card getRandom(){
    Random rand = new Random();
    return this.card[rand.nextInt(pointer + 1)];
  }
  
  public int getLength(){
    return this.pointer;
  }

  
  
  // replace at index,given an new element
  public String toString(){
    String s = "";
    for (int i = 0; i<this.pointer; i++)
      {
        s += "" + (i+1) + ": " + this.card[i] + "\n";
      }
    return s;
  }
   // shuffle cards function
	public void shuffleCards(){
		
		Random rand = new Random();
		
		for ( int i=0; i<this.card.length;  i++){
			int randomIndexSwap= rand.nextInt(card.length);
			Card temp = card[randomIndexSwap];
			card[randomIndexSwap] = card[i];
			card[i]= temp;
			
		}
		
	}
}
