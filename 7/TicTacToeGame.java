import java.util.Scanner;
public class TicTacToeGame{
	public static void main(String[] arg){
	System.out.println("Welcome to tictactoe Gaming!");
		Board board= new Board();
		
		boolean gameOver= false;
		int player =1;
		
		Square playerToken =Square.X;
		
		while(!gameOver){
			System.out.println(board);
			if(player==1){
				playerToken=Square.X;
				
			}
			else{
				playerToken=Square.O;
			}
			Scanner input= new Scanner(System.in);
			System.out.println("enter number of row you want to place your token: ");
			int row = input.nextInt();
			
			System.out.println("enter number of column you want to put your token: ");
			int col = input.nextInt();
			
			while(!board.placeToken(row,col, playerToken)){
					
			System.out.println("enter number of row you want to place your token: ");
			row = input.nextInt();
			
			System.out.println("enter number of column you want to put your token: ");
			col = input.nextInt();
			
			}
			if(board.checkIfFull()){
				System.out.println("It's tie!");
				gameOver= true;
			}
			else if(board.checkIfWinning(playerToken)){
				
				System.out.println("Player "+player+" is the winner");
				gameOver= true;
				
			}
			else{
				player= player+1;
				if(player>2){
					player=1;
				}
			}
			
		}
	}
}