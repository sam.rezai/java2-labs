public class Board{
	private Square[][] tictactoe;
	
	
	public Board(){
		
	tictactoe = new Square[3][3];
	
	for (int i=0;i<tictactoe.length;i++){
		
		for(int j=0; j<tictactoe[i].length;j++){
			
			tictactoe[i][j]= Square.BLANK;
			
			}
		}
	}
	public String toString(){
		
		String board="";
		
		for(int i=0;i<tictactoe.length;i++){
			
			for(int j=0; j<tictactoe[i].length;j++){
				
				board+= tictactoe[i][j]+",";
				
			}
			board+= "\n";
		}
		
		return board;
			
		
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		
		
		if(row>=0 && row<=2 && col>=0 && col<=2 && tictactoe[row][col]== Square.BLANK){
			
			tictactoe[row][col]= playerToken;
			
			return true;
		}
		return false;
		
	}
	public boolean checkIfFull(){
		
		for(int i=0;i<tictactoe.length;i++){
			
			for(int j=0; j<tictactoe[i].length;j++){
		
				if(tictactoe[i][j]== Square.BLANK){
				return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		
			for(int i=0;i<tictactoe.length;i++){
			
			if(tictactoe[i][0]== playerToken && tictactoe[i][1]== playerToken && tictactoe[i][2]== playerToken){
				
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningFVertical(Square playerToken){
		
		for(int j=0; j<tictactoe.length;j++){
			
			if(tictactoe[0][j]== playerToken && tictactoe[1][j]== playerToken && tictactoe[2][j]== playerToken){
				return true;
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken){
		
		if(checkIfWinningHorizontal(playerToken)){
			return true;
		}
		if(checkIfWinningFVertical(playerToken)){
			return true;
		}
		return false;
		
	}
	//public checkIfWinningDiagonal(Square playerToken){
		
	//}
	
}