public class MethodsTest{
	public static void main(String[] args){
		int x= 5;
		System.out.println(x);
		menthodNoInputNoReturn();
		System.out.println(x);
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(x,7);
		int z= methodNoInputReturnInt();
		System.out.println(z);
		double sqrtRoot= sumSquareRoot(9,5);
		System.out.println(sqrtRoot);
		
		String s1="java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		int one= sc.addOne(50);
		System.out.println(one);
		int two= sc.addTwo(50);
		System.out.println(two);
		
	}
	public static void menthodNoInputNoReturn(){
		System.out.println("i'm in a method that takes no input and returns nothing");
		int x= 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int input){
		System.out.println("Inside the method one input no return");
		System.out.println(input-5);
		System.out.println(input);
		
	}
	public static void methodTwoInputNoReturn(int input1, double input2){
		System.out.println("first input "+input1+" and second input is "+input2);
	}
	public static int methodNoInputReturnInt(){
		int input= 5;
		return input;
	}
	public static double sumSquareRoot(int a, int b){
		
		int thirdValue= a + b;
		double finalValue = Math.sqrt(thirdValue);
		
		return finalValue;
	}
}
