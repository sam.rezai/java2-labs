import java.util.Scanner;
import java.util.Random;
public class Main{
	public static void main(String[] args){
		
			Scanner reader= new Scanner(System.in);
		
			
			Random rand = new Random();
			
			int playerTurn =0;
			
			boolean gameDirection = false;
			
			DynamicCardArray deck = generateDeck();
			System.out.println(deck);
			
			deck.shuffleCards();
			
			//System.out.println(deck);
			
			String[] playersName= new String[3];
			
			
			for (int i =1; i<= playersName.length; i++){
				
				System.out.println("Please enter your name");
				
				playersName[i-1]= reader.nextLine();
			}
				//Make an array of players
			DynamicCardArray[] players= new DynamicCardArray[playersName.length];
			
			for (int i=0; i< playersName.length;i++){
				
				players[i]= new DynamicCardArray();
		
			}
			for (int i =0; i<players.length; i++){
				
				for (int j=1; j<=7;j++){
					
					players[i].add(deck.removeAtIndex(rand.nextInt(deck.getLength()+1)));	
				}
				System.out.println(playersName[i]+ " is the "+(i+1)+" player and here is the hands of this player"+players[i]);
				
			}
			
			boolean gameOver = false;
			DynamicCardArray discard = new DynamicCardArray();
				
				discard.add(deck.removeAtIndex(rand.nextInt(deck.getLength())));//give one card from the deck to the discard (much like how we did it for the hands but just once!)
				
				System.out.println(discard);
				
			while(!gameOver){
				
				System.out.println("Top discard pile is:  "+discard.getTopCard()); // I added this here from before the loop so that it prints everytime the player turn changes  S>O>P
				

				if(!gameDirection && playerTurn >= playersName.length){

					playerTurn=playerTurn-playersName.length; //This is to make sure that the array does not go out of bound and after the last player, it comes back to the 1st
					//it looks like this becuase of SKIP
					//if 3rd player skips, playerTurn becomes 4, but we want it to be 1 (skipping 0)
				}
				if(gameDirection && (playerTurn < 0)){
					playerTurn = playerTurn+playersName.length;
				}
				int originalPlayerTurn = playerTurn;
				System.out.println("Player " + (playerTurn+1) + " what card do you want to play?"); //Using the variable playerTurn here for makes it easier to change S.O.P
				//we take the user input, and use the removeAtIndex
				
				System.out.println(players[playerTurn]);
				int userInput = reader.nextInt();

			
				Card potentialCard = players[playerTurn].removeAtIndex(userInput-1);

				Card topDiscard = discard.getTopCard();//removes the last card of the discard pile
				
				if (potentialCard.getValue() == Value.DRAWTWO && potentialCard.getColor() == topDiscard.getColor()){
					int next = 0;
					if(!gameDirection && playerTurn == players.length-1){
						next = 0;
					}
					else if(gameDirection && playerTurn == 0){
						next = players.length-1;
					}
					else if(!gameDirection){
						next = playerTurn+1;
					}
					else if(gameDirection){
						next = playerTurn-1;
					}
					players[next].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					players[next].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					discard.add(potentialCard);
				}
				else if (potentialCard.getValue() == Value.DRAWFOUR){
					int next = 0;
					if(!gameDirection && playerTurn == players.length-1){
						next = 0;
					}
					else if(gameDirection && playerTurn == 0){
						next = players.length-1;
					}
					else if(!gameDirection){
						next = playerTurn+1;
					}
					else if(gameDirection){
						next = playerTurn-1;
					}
				
					players[next].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					players[next].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					players[next].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					players[next].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					discard.add(potentialCard);

				}
				else if(potentialCard.getValue() == Value.SKIP && topDiscard.getColor() == potentialCard.getColor()){
					
					if (!gameDirection){
				
						
						playerTurn++;
		
					}
					else {
						
						playerTurn--;
							
					}
					discard.add(potentialCard);
				}
				else if( potentialCard.getValue()== Value.REVERSE && (potentialCard.getColor() == topDiscard.getColor() || topDiscard.getValue() == Value.REVERSE)){
					
					DynamicCardArray[] oldOrder = new DynamicCardArray[players.length];
					for(int i=0; i<players.length;i++){
						oldOrder[i] = players[i];
					}
					for(int i = players.length-1; i>=0;i--){
						players[i] = oldOrder[i];
					}
					gameDirection = true;
					discard.add(potentialCard);
					
				}
				else if (potentialCard.getColor() == topDiscard.getColor() || potentialCard.getValue() == topDiscard.getValue()){
					//then its good to good
					
					discard.add(potentialCard);
					
					System.out.println("The card you have placed on discardPile is: " +discard.getTopCard());
					
				}
				else{
					System.out.println("the selected card cannot be played, draw one");
					players[playerTurn].add(deck.removeAtIndex(rand.nextInt(deck.getLength())));
					players[playerTurn].add(potentialCard);// puts back the player hand in its place
					
					
				}
				gameOver = isGameOver(players[originalPlayerTurn]);

				if(gameDirection){
					playerTurn--;
				}
				else{
					playerTurn++;
				}
			}
	}
	
	public static boolean isGameOver(DynamicCardArray player){
		if(player.getLength() == 0){
			return true;
		}
		return false;
	}
		
	//ADD the generate deck function
	public static DynamicCardArray generateDeck(){

		DynamicCardArray deck = new DynamicCardArray();

		Color[] color = Color.values();
		Value[] value = Value.values();

		for(Color c :color){

			for (int i = 0; i<value.length; i++){

				deck.add(new Card(value[i],c));

				if (value[i] != Value.ZERO){
					deck.add(new Card(value[i],c));
				}

			}
		}
		return deck;
		
	
	}

}